﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Improvements : MonoBehaviour {
    // protected enum EnumImprovement { Gun, Bullet, Jetpack, Coin }

    [System.Serializable]
    private class ImproveUI {
        public Button btnSelect;
        public Image content;
        public Button btnBuy;
        public TextMeshProUGUI txtCost;
        public TextMeshProUGUI txtLvl;
    }

    public RectTransform rectT;
    [SerializeField] ImproveUI impGun;
    [SerializeField] ImproveUI impBullet;
    [SerializeField] ImproveUI impJetpack;
    [SerializeField] ImproveUI impCoin;

    ExtraEnums.EnumImprovement currentImprovement;

    void Awake () {
        SetUI ();
        SetOnClickButtons ();
    }

    void SetUI () {
        rectT = GetComponent<RectTransform> ();

        var contentParent = transform.GetChild (0);
        var btnParent = transform.GetChild (1);

        impGun.content = contentParent.GetChild (0).GetComponent<Image> ();
        impBullet.content = contentParent.GetChild (1).GetComponent<Image> ();
        impJetpack.content = contentParent.GetChild (2).GetComponent<Image> ();
        impCoin.content = contentParent.GetChild (3).GetComponent<Image> ();

        impGun.btnSelect = btnParent.GetChild (0).GetComponent<Button> ();
        impBullet.btnSelect = btnParent.GetChild (1).GetComponent<Button> ();
        impJetpack.btnSelect = btnParent.GetChild (2).GetComponent<Button> ();
        impCoin.btnSelect = btnParent.GetChild (3).GetComponent<Button> ();

        impGun.btnBuy = impGun.content.transform.GetChild (0).GetComponent<Button> ();
        impBullet.btnBuy = impBullet.content.transform.GetChild (0).GetComponent<Button> ();
        impJetpack.btnBuy = impJetpack.content.transform.GetChild (0).GetComponent<Button> ();
        impCoin.btnBuy = impCoin.content.transform.GetChild (0).GetComponent<Button> ();

        impGun.txtCost = impGun.btnBuy.transform.GetChild (1).GetComponent<TextMeshProUGUI> ();
        impBullet.txtCost = impBullet.btnBuy.transform.GetChild (1).GetComponent<TextMeshProUGUI> ();
        impJetpack.txtCost = impJetpack.btnBuy.transform.GetChild (1).GetComponent<TextMeshProUGUI> ();
        impCoin.txtCost = impCoin.btnBuy.transform.GetChild (1).GetComponent<TextMeshProUGUI> ();

        impGun.txtLvl = impGun.content.transform.GetChild (2).GetComponent<TextMeshProUGUI> ();
        impBullet.txtLvl = impBullet.content.transform.GetChild (2).GetComponent<TextMeshProUGUI> ();
        impJetpack.txtLvl = impJetpack.content.transform.GetChild (2).GetComponent<TextMeshProUGUI> ();
        impCoin.txtLvl = impCoin.content.transform.GetChild (2).GetComponent<TextMeshProUGUI> ();
    }

    void SetOnClickButtons () {
        impGun.btnSelect.onClick.AddListener (() => ChangeImprove (ExtraEnums.EnumImprovement.Gun));
        impBullet.btnSelect.onClick.AddListener (() => ChangeImprove (ExtraEnums.EnumImprovement.Bullet));
        impJetpack.btnSelect.onClick.AddListener (() => ChangeImprove (ExtraEnums.EnumImprovement.Jetpack));
        impCoin.btnSelect.onClick.AddListener (() => ChangeImprove (ExtraEnums.EnumImprovement.Coin));

        impGun.btnBuy.onClick.AddListener (() => GameManager.Instance.Main.UpgradeModifier (ExtraEnums.EnumImprovement.Gun));
        impBullet.btnBuy.onClick.AddListener (() => GameManager.Instance.Main.UpgradeModifier (ExtraEnums.EnumImprovement.Bullet));
        impJetpack.btnBuy.onClick.AddListener (() => GameManager.Instance.Main.UpgradeModifier (ExtraEnums.EnumImprovement.Jetpack));
        impCoin.btnBuy.onClick.AddListener (() => GameManager.Instance.Main.UpgradeModifier (ExtraEnums.EnumImprovement.Coin));
    }

    public void StateButton(ExtraEnums.EnumImprovement imp, bool value){
        switch (imp) {
            case ExtraEnums.EnumImprovement.Gun:
                impGun.btnBuy.interactable = value;
                break;
            case ExtraEnums.EnumImprovement.Bullet:
                impBullet.btnBuy.interactable = value;
                break;
            case ExtraEnums.EnumImprovement.Jetpack:
                impJetpack.btnBuy.interactable = value;
                break;
            case ExtraEnums.EnumImprovement.Coin:
                impCoin.btnBuy.interactable = value;
                break;
        }
    }

    public void SetImproveValues(ExtraEnums.EnumImprovement imp, int cost, int lvl){ //Set Cost and Lvl
        switch (imp) {
            case ExtraEnums.EnumImprovement.Gun:
                impGun.txtCost.text = UtilityHelper.AdjustBigValue(cost);
                impGun.txtLvl.text = $"Nivel {lvl}";
                break;
            case ExtraEnums.EnumImprovement.Bullet:
                impBullet.txtCost.text = UtilityHelper.AdjustBigValue(cost);
                impBullet.txtLvl.text = $"Nivel {lvl}";
                break;
            case ExtraEnums.EnumImprovement.Jetpack:
                impJetpack.txtCost.text = UtilityHelper.AdjustBigValue(cost);
                impJetpack.txtLvl.text = $"Nivel {lvl}";
                break;
            case ExtraEnums.EnumImprovement.Coin:
                impCoin.txtCost.text = UtilityHelper.AdjustBigValue(cost);
                impCoin.txtLvl.text = $"Nivel {lvl}";
                break;
        }
    }

    void ChangeImprove (ExtraEnums.EnumImprovement imp) {
        ViewCurrentImprove (false);
        currentImprovement = imp;
        ViewCurrentImprove (true);
    }

    void ViewCurrentImprove (bool value) {
        switch (currentImprovement) {
            case ExtraEnums.EnumImprovement.Gun:
                impGun.content.gameObject.SetActive (value);
                break;
            case ExtraEnums.EnumImprovement.Bullet:
                impBullet.content.gameObject.SetActive (value);
                break;
            case ExtraEnums.EnumImprovement.Jetpack:
                impJetpack.content.gameObject.SetActive (value);
                break;
            case ExtraEnums.EnumImprovement.Coin:
                impCoin.content.gameObject.SetActive (value);
                break;
        }
    }

    void BuyImprove(ExtraEnums.EnumImprovement imp){
        GameManager.Instance.Main.UpgradeModifier (imp);
    }
}
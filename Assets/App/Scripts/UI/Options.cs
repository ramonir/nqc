﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    [Header("Options")]
    [SerializeField] Button btnMenu;
    [SerializeField] Button shadowMenu;
    [SerializeField] Image imgMenu;
    [SerializeField] float movespeed;
    bool menu;

    private void Awake()
    {
        btnMenu.onClick.AddListener(ButtonMenu);
        shadowMenu.onClick = btnMenu.onClick;
    }

    void ButtonMenu()
    {
        menu = !menu;
        switch (menu)
        {
            case true:
                shadowMenu.gameObject.SetActive(true);
                break;
            case false:
                shadowMenu.gameObject.SetActive(false);
                break;
        }
    }

    private void Update()
    {
        PositionMenu();
    }

    void PositionMenu(){
        var pos = imgMenu.rectTransform.anchoredPosition;
        switch (menu)
        {
            case true:
                pos[0] = Mathf.Lerp(pos.x, 0, Time.deltaTime * movespeed);
                break;
            case false:
                pos[0] = Mathf.Lerp(pos.x, 300, Time.deltaTime * movespeed);
                break;
        }
        imgMenu.rectTransform.anchoredPosition = pos;
    }
}

﻿using App.Core;
using App.Enemys;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Bullets {
    public class ColliderBullet : Bullet {

        protected override void OnEnable() {
            base.OnEnable();
            GameManager.Instance.ObjectPooler.Recicle(gameObject, timeDie);
        }

        public override void Move() {
            transform.Translate(Vector2.up * speed * Time.deltaTime);
        }

        public override void OnTriggerEnter2D(Collider2D c) {
            base.OnTriggerEnter2D(c);
            var enemy = c.GetComponent<Enemy>();
            if (enemy) {
                if (enemyBullet) return;
                // life = 0;
                Hit(999);
            }
            var bullet = c.GetComponent<Bullet>();
            if (bullet) {
                if (bullet.enemyBullet == enemyBullet) return;
                bullet.Hit(damage);
                // bullet.life -= damage;
                // life -= bullet.damage;
                Hit(bullet.damage);
            }
        }
    }
}

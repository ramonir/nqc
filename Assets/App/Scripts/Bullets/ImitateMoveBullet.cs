﻿using App.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Bullets {
    public class ImitateMoveBullet : Bullet {

        protected override void OnEnable() {
            base.OnEnable();
            GameManager.Instance.ObjectPooler.Recicle(gameObject, timeDie);
        }

        public override void Move() {
            var PosX = transform.position;
            PosX[0] = GameManager.Instance.Player.transform.position.x;
            transform.position = PosX;
            transform.Translate(Vector2.up * speed * Time.deltaTime);
        }

    }
}

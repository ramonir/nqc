﻿using App.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Bullets {
    public class AroundBullet : Bullet {

        [Header("Exclusive")]
        Vector2 startPos;
        
        protected override void OnEnable() {
            base.OnEnable();
            startPos = transform.position;
            GameManager.Instance.ObjectPooler.Recicle(gameObject, timeDie);
            // transform.SetParent(GameManager.Instance.Player.transform);
        }

        public override void Move() {
            // transform.RotateAround(GameManager.Instance.Player.transform.position, new Vector3(0,0,1), speed*5);
            transform.RotateAround(startPos, new Vector3(0,0,1), speed*5);
            transform.Translate(Vector2.up * (speed/5) * Time.deltaTime);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;

namespace App.Bullets {
    public class BugBullet : Bullet {

        protected override void OnEnable() {
            base.OnEnable();
            GameManager.Instance.ObjectPooler.Recicle(gameObject, timeDie);
        }

        public override void Move() {
            transform.Translate(Vector2.up * speed * Time.deltaTime);
            float rndRotation = Random.Range(-25, 25);
            transform.Rotate(0, 0, rndRotation);
        }

    }
}

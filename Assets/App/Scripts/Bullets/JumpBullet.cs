﻿using App.Core;
using App.Enemys;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Bullets {
    public class JumpBullet : Bullet {

        [SerializeField] float jumpTimeDie;

        protected override void OnEnable() {
            base.OnEnable();
            timeDie = startTimeDie;
        }

        public override void Move() {
            timeDie -= Time.deltaTime;
            if (timeDie <= 0) GameManager.Instance.ObjectPooler.Recicle(gameObject);
            transform.Translate(Vector2.up * speed * Time.deltaTime);
        }

        void ChangeDirection() {
            var rnd = Random.Range(0, 360);
            transform.Rotate(0, 0, rnd);
        }

        public override void OnTriggerEnter2D(Collider2D c) {
            base.OnTriggerEnter2D(c);
            var enemy = c.GetComponent<Enemy>();
            if (enemy) {
                if (enemyBullet) return;
                timeDie = jumpTimeDie;
                ChangeDirection();
            }
        }
    }
}

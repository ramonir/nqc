﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;

namespace App.Bullets {
    public class BoomerangBullet : Bullet {

        [Header("Exclusive")]
        [SerializeField] float backForce = 3;
        bool returnBullet;

        protected override void OnEnable () {
            base.OnEnable ();
            GameManager.Instance.ObjectPooler.Recicle (gameObject, timeDie);

            Boomerang ();
            var rnd = Random.Range (-10, 10);
            transform.Rotate (0, 0, rnd);
            speed = Random.Range (speed, speed + 1);
        }

        public override void Move () {
            transform.Translate (Vector2.up * speed * Time.deltaTime);
            speed -= Time.deltaTime * backForce;
        }

        async void Boomerang () {
            await new WaitUntil (() => speed < 0);
            var rnd = Random.Range (-10, 10);
            transform.Rotate (0, 0, rnd);
        }
    }
}
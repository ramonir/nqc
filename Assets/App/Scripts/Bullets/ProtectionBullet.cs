﻿using App.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Bullets {
    public class ProtectionBullet : Bullet {

        protected override void OnEnable() {
            base.OnEnable();
            GameManager.Instance.ObjectPooler.Recicle(gameObject, timeDie);
            transform.SetParent(GameManager.Instance.Player.transform);
        }

        public override void Move() {
            transform.RotateAround(GameManager.Instance.Player.transform.position, new Vector3(0,0,1), speed*5);
            transform.Translate(Vector2.up * (speed/5) * Time.deltaTime);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;

namespace App.Bullets {
    public class ShotGunBullet : Bullet {

        [Header ("Exclusive")]
        [SerializeField] int limitAngle;
        [SerializeField] float timeDieMax;
        [SerializeField] int fragmentAmount;
        bool fragment;
        GameObject _bullet;
        Bullet _bulletParent;
        Vector2 _scale;

        protected override void OnEnable () {
            base.OnEnable ();
            fragment = false;

            if (transform.parent) {
                fragment = true;

                _bulletParent = transform.parent.GetComponentInParent<Bullet> ();
                StartCoroutine (Coroutine_SetStatus ());

                transform.SetParent (null);
            }

            if (!fragment) {
                _bullet = Resources.Load ("Prefabs/Bullet/" + this.GetType ().Name.ToString ()) as GameObject;

                Hit (999);
            }
        }

        IEnumerator Coroutine_SetStatus () {
            yield return new WaitForEndOfFrame ();
            speed = _bulletParent.speed;
            damage = _bulletParent.damage;
            life = _bulletParent.life;
            timeDie = _bulletParent.timeDie;
            _scale = _bulletParent.gameObject.transform.localScale;

            var scale = transform.localScale;
            scale[0] = _scale.x;
            scale[1] = _scale.y;
            transform.localScale = scale;

            float rndDieTime = Random.Range (timeDie, timeDieMax);
            GameManager.Instance.ObjectPooler.Recicle (gameObject, rndDieTime);

            float rndRotation = Random.Range (-limitAngle, limitAngle);
            transform.Rotate (0, 0, rndRotation);

            float rndSpeed = Random.Range (speed, speed * 2);
            speed = rndSpeed;
        }

        public override void Move () {
            transform.Translate (Vector2.up * speed * Time.deltaTime);
        }

        public override void Die () {
            if (life > 0) return;
            if (!fragment) {
                SpawnFragment ();
            }
            base.Die ();
        }

        void SpawnFragment () {
            for (int i = 0; i < fragmentAmount; i++) {
                var go = GameManager.Instance.ObjectPooler.SpawnFromPool (_bullet, transform.position, transform.rotation, transform);
                var goBullet = go.GetComponent<ShotGunBullet> ();
                goBullet.enemyBullet = enemyBullet;
            }
        }
    }
}
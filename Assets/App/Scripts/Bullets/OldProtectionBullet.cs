using App.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Bullets {
    public class OldProtectionBullet : Bullet {

        [SerializeField] float rotation;
        float _rot;
        bool _rnd;

        // protected override void Start() {
        //     base.Start();
        //     Destroy(gameObject, timeDie);
        //     _rnd = (Random.Range(0, 2) == 0);
        //     if(_rnd) _rot = 5;
        //     else _rot = -5;
        //     transform.SetParent(GameManager.Instance.Player.transform);
        // }

        protected override void OnEnable() {
            base.OnEnable();
            GameManager.Instance.ObjectPooler.Recicle(gameObject, timeDie);

            _rnd = (Random.Range(0, 2) == 0);
            if(_rnd) _rot = rotation;
            else _rot = -rotation;
            transform.SetParent(GameManager.Instance.Player.transform);
        }

        // void Update() {
        //     transform.Translate(Vector2.up * speed * Time.deltaTime);
        //     if (_rnd) _rot -= Time.deltaTime;
        //     else _rot += Time.deltaTime;
        //     transform.Rotate(0, 0, _rot);
        // }

        public override void Move() {
            transform.Translate(Vector2.up * speed * Time.deltaTime);
            if (_rnd) _rot -= Time.deltaTime;
            else _rot += Time.deltaTime;
            transform.Rotate(0, 0, _rot);
        }

    }
}

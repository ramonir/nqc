﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;

namespace App.Bullets {
    public class ExpansionBullet : Bullet {

        // public float slowShotSpeed;

        protected override void OnEnable () {
            base.OnEnable ();
            GameManager.Instance.ObjectPooler.Recicle (gameObject, timeDie);
        }

        public override void Move () {
            transform.Translate (Vector2.up * speed * Time.deltaTime);
            var scale = transform.localScale;
            scale[0] += Time.deltaTime / 5;
            scale[1] += Time.deltaTime / 5;
            transform.localScale = scale;
            damage += damage * (Time.deltaTime / 5);
        }

    }
}
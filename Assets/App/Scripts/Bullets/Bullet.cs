﻿using App.Core;
using App.Enemys;
using App.Guns;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Bullets {
    public abstract class Bullet : MonoBehaviour {

        public bool enemyBullet;
        public int lvl;
        [SerializeField] protected float startSpeed;
        public float speed;
        [SerializeField] protected float startDamage;
        public float damage;
        [SerializeField] protected float startLife;
        public float life;
        [SerializeField] protected float startTimeDie;
        public float timeDie;
        [SerializeField] protected Vector2 startScale;

        protected virtual void Start() {
            damage += damage * GameManager.Instance.Main.ImpDamageBullet.Modifier;
        }

        protected virtual void OnEnable() {
            speed = startSpeed;
            damage = startDamage;
            life = startLife;
            timeDie = startTimeDie;

            var scale = transform.localScale;
            scale[0] = startScale.x;
            scale[1] = startScale.y;
            transform.localScale = scale;
        }

        private void Update() {
            Move();
        }

        abstract public void Move();

        public virtual void Die() {
            if (life > 0) return;
            // Destroy(gameObject);
            gameObject.SetActive(false);
        }

        public void Hit(float hit = 1){
            life = life - hit;
            Die();
        }

        public virtual void OnTriggerEnter2D(Collider2D c) {
            var enemy = c.GetComponent<Enemy>();
            if (enemy) {
                if (enemyBullet) return;
                enemy.Hit(damage);
                Hit();
                // life--;
            }
            var player = c.GetComponent<Player>();
            if (player) {
                if (!enemyBullet) return;
                player.Hit(damage);
                Hit();
                // player.life -= damage;
                // life--;
            }
            var gun = c.GetComponent<Gun>();
            if (gun) {
                if (!enemyBullet) return;
                if (gun.indestructible) return;
                gun.Hit(damage);
                Hit();
                // gun.life -= damage;
                // life--;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;

namespace App.Bullets {

    public class BombBullet : Bullet {

        [Header ("Exclusive")]
        private float startRadius;
        [SerializeField] private float explosionRadius;
        [SerializeField] private float explosionTime;

        protected override void Start () {
            base.Start ();
            startRadius = GetComponent<CircleCollider2D> ().radius;
        }
        protected override void OnEnable () {
            base.OnEnable ();

            GetComponent<CircleCollider2D> ().radius = startRadius;
            Explosion ();
        }

        async void Explosion () {
            await new WaitForSeconds (timeDie);
            Hit (999);
        }

        public override void Move () {
            transform.Translate (Vector2.up * speed * Time.deltaTime);
        }

        public async override void Die () {
            if (life > 0) return;
            GetComponent<CircleCollider2D> ().radius = explosionRadius;
            await new WaitForSeconds (explosionTime);
            gameObject.SetActive (false);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;

namespace App.Bullets {
    public class FragmentBullet : Bullet {

        [Header ("Exclusive")]
        // [SerializeField] float fragmentStartScale; // 1.5
        [SerializeField] float fragmentLife;
        // [SerializeField] float fragmentTimeDie;
        [SerializeField] Transform firstPoint;
        [SerializeField] Transform secondPoint;
        [SerializeField] Transform thirdPoint;
        bool fragment;
        GameObject _bullet;
        Bullet _bulletParent;
        Vector2 _scale;

        protected override void OnEnable () {
            base.OnEnable ();
            fragment = false;

            if (transform.parent) {
                fragment = true;

                _bulletParent = transform.parent.GetComponentInParent<Bullet> ();
                StartCoroutine (Coroutine_SetStatus ());

                transform.SetParent (null);
            }

            if (!fragment) {
                _bullet = Resources.Load ("Prefabs/Bullet/" + this.GetType ().Name.ToString ()) as GameObject;

                GameManager.Instance.ObjectPooler.Recicle (gameObject, timeDie);
            }
        }

        IEnumerator Coroutine_SetStatus () {
            yield return new WaitForEndOfFrame ();
            speed = _bulletParent.speed;
            damage = _bulletParent.damage;
            life = _bulletParent.life;
            timeDie = _bulletParent.timeDie;
            _scale = _bulletParent.gameObject.transform.localScale;

            GameManager.Instance.ObjectPooler.Recicle (gameObject, timeDie / 3);

            life = fragmentLife;

            var scale = transform.localScale;
            scale[0] = _scale.x / 1.5f;
            scale[1] = _scale.y / 1.5f;
            transform.localScale = scale;
        }

        public override void Move () {
            transform.Translate (Vector2.up * speed * Time.deltaTime);
        }

        public override void Die () {
            if (life > 0) return;
            if (!fragment) {
                SpawnFragment (firstPoint);
                SpawnFragment (secondPoint);
                SpawnFragment (thirdPoint);
            }
            base.Die ();
        }

        void SpawnFragment (Transform point) {
            var go = GameManager.Instance.ObjectPooler.SpawnFromPool (_bullet, point.position, point.rotation, point);
            var goBullet = go.GetComponent<FragmentBullet> ();
            goBullet.enemyBullet = enemyBullet;
        }
    }
}
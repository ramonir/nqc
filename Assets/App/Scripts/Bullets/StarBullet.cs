﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;

namespace App.Bullets {
    public class StarBullet : Bullet {

        [Header ("Exclusive")]
        [SerializeField] float fragmentStartScale;
        [SerializeField] float fragmentLife;
        [SerializeField] float fragmentTimeDie;
        [SerializeField] Transform[] points;
        bool fragment;
        GameObject _bullet;
        Bullet _bulletParent;
        Vector2 _scale;

        protected override void OnEnable () {
            base.OnEnable ();
            fragment = false;

            if (transform.parent) {
                fragment = true;

                _bulletParent = transform.parent.GetComponentInParent<Bullet> ();
                StartCoroutine (Coroutine_SetStatus ());

                transform.SetParent (null);
            }

            if (!fragment) {
                _bullet = Resources.Load ("Prefabs/Bullet/" + this.GetType ().Name.ToString ()) as GameObject;

                FragmentObject (timeDie);
            }
        }

        IEnumerator Coroutine_SetStatus () {
            yield return new WaitForEndOfFrame ();
            speed = _bulletParent.speed;
            damage = _bulletParent.damage;
            life = _bulletParent.life;
            timeDie = _bulletParent.timeDie;
            _scale = _bulletParent.gameObject.transform.localScale;

            GameManager.Instance.ObjectPooler.Recicle (gameObject, fragmentTimeDie);

            life = fragmentLife;

            var scale = transform.localScale;
            scale[0] = _scale.x / 2;
            scale[1] = _scale.y / 2;
            transform.localScale = scale;
        }

        public override void Move () {
            transform.Translate (Vector2.up * speed * Time.deltaTime);
        }

        async public void FragmentObject (float time) {
            await new WaitForSeconds (time);
            SpawnFragment (points);
            GameManager.Instance.ObjectPooler.Recicle (gameObject);
        }

        void SpawnFragment (Transform[] points) {
            for (int i = 0; i < points.Length; i++) {
                var go = GameManager.Instance.ObjectPooler.SpawnFromPool (_bullet, points[i].position, points[i].rotation, points[i]);
                var goBullet = go.GetComponent<StarBullet> ();
                goBullet.enemyBullet = enemyBullet;
            }
        }
    }
}
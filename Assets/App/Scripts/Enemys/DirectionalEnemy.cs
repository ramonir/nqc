﻿using App.Core;
using UnityEngine;
using UnityEngine.UI;
namespace App.Enemys {
    public class DirectionalEnemy : Enemy {

        private void Start() {
            Vector3 dir = GameManager.Instance.Player.transform.position - transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 270;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
namespace App.Enemys {
    public class HorizontalEnemy : Enemy {

        public float horizontalSpeed;
        public bool moveRight;
        public bool oneDirection;

        void Update () {
            if (moveRight) transform.Translate (Vector2.right * horizontalSpeed * Time.deltaTime);
            else transform.Translate (Vector2.left * horizontalSpeed * Time.deltaTime);
        }

        public void OnTriggerEnter2D (Collider2D c) {
            if (c.tag == "WALL_LEFT" || c.tag == "WALL_RIGHT") {
                if(oneDirection) return;
                moveRight = !moveRight;
            }
        }
    }
}
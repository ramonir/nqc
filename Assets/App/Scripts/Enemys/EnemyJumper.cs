﻿using System.Collections;
using System.Collections.Generic;
using App.Bullets;
using App.Core;
using App.Enemys;
using UnityEngine;

public class EnemyJumper : Enemy {

    [Header("Start")]


    [Header ("Ground Collision")]
    public Vector2 variantJumpForce = new Vector2 (9f, 11f);

    [Header ("Wall Collision")]
    public Vector2 variantWallJumpForce = new Vector2 (0.5f, 1);
    public Vector2 variantWallForce = new Vector2 (0.1f, 0.5f);
    public bool keepWallForce = true;
    float _wallForce;

    protected override void OnEnable () {
        base.OnEnable();
        StartSizeAndSpeed ();
        StartSideAndVelocity ();
        StartStatus ();
    }

    void StartSizeAndSpeed () {
        this.transform.localScale = Vector3.one;
        this.speed = startSpeed;

        var variant = Random.Range (1f, 2f);
        this.transform.localScale *= variant;
        var convertVariant = 1f - (variant / 10f);
        this.speed = convertVariant * this.speed;
    }

    void StartSideAndVelocity () {
        int randomSide = Random.Range (-1, 2);
        while (randomSide == 0) randomSide = Random.Range (-1, 2);
        ModifyVelocity (variantWallForce * randomSide, variantWallForce);
    }

    void StartStatus () {
        _startLife = startLife;

        Rb.gravityScale = this.speed;
        var baseLife = startLife;
        var variant = Random.Range (0.09f, 0.21f);
        var level = GameManager.Instance.Main.Level;
        _startLife = (baseLife * ((variant * level) * level)) + (baseLife * (level + 1));
        variant = Random.Range (-2, 2);
        _startLife -= variant;
        life = _startLife;

        ModifyLifeText(life);
    }

    private void Update () {
        if (keepWallForce) ModifyVelocity (_wallForce, 0);
    }

    public void ModifyVelocity (float x, float y) {
        if (x == 0) x = _wallForce;
        if (y == 0) y = Rb.velocity.y;
        _wallForce = x;
        Rb.velocity = new Vector2 (x, y);
    }

    public void ModifyVelocity (Vector2 xVariant, Vector2 yVariant) {
        float x = Random.Range (xVariant.x, xVariant.y);
        float y = Random.Range (yVariant.x, yVariant.y);
        if (x == 0) x = _wallForce;
        if (y == 0) y = Rb.velocity.y;
        _wallForce = x;
        Rb.velocity = new Vector2 (x, y);
    }

    public void OnTriggerEnter2D (Collider2D c) {
        if (c.tag == "GROUND") {
            // rb.AddForce(new Vector2(0,jumpForce));
            // rb.velocity = new Vector2(0, jumpForce);
            // ModifyVelocity(Vector2.zero, variantJumpForce);
            ModifyVelocity (Vector2.zero, variantJumpForce);
        } else if (c.tag == "WALL_LEFT") {
            // rb.AddForce(new Vector2(0,jumpForce));
            // rb.velocity = new Vector2(0, jumpForce);
            ModifyVelocity (variantWallForce, variantWallJumpForce);
        } else if (c.tag == "WALL_RIGHT") {
            // rb.AddForce(new Vector2(0,jumpForce));
            // rb.velocity = new Vector2(0, jumpForce);
            ModifyVelocity (-variantWallForce, variantWallJumpForce);
        }
        var bullet = c.GetComponent<Bullet> ();
        if (bullet) {
            var force = 10 * bullet.damage;
            if (Rb.velocity.y > -1) return;
            Rb.AddForce (new Vector2 (0, force));
        }
    }
}
﻿using App.Core;
using App.Structures;
using UnityEngine;
using UnityEngine.UI;
namespace App.Enemys {
    public abstract class Enemy : Structure {
        
        [Header("Enemy")]
        [SerializeField] protected float startSpeed = 0.25f;
        [SerializeField] protected float speed = 0.25f;
        [SerializeField] private float colDamage = 1;
        public float ColDamage { get => colDamage; }
        protected float _startLife;
        [SerializeField] protected bool isBoss = false;

        protected override void Die() {
            if ((int)life > 0) return;
            base.Die();
            GameManager.Instance.Main.GainScore(_startLife, !isBoss);
            if (isBoss) GameManager.Instance.FinishBoss();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Item", order = 1)]
public class Item : ScriptableObject
{
        [System.Serializable]
        public class ItemProperty {
            public GameObject Item;
            public bool have;
        }

        public enum ItemType{
            Gun,
            Bullet,
            Structure
        }

    public ItemType type;
    public List<ItemProperty> items;
    [SerializeField]
    public GameObject[] HaveItems { get ; set; }
    public float baseChance;
    [SerializeField]
    Vector2 _chance;
    public Vector2 Chance { get ; set; }
}

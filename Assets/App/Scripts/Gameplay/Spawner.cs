﻿using System.Collections;
using System.Collections.Generic;
using App.Bullets;
using App.Core;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject baseEnemyJumper;
    public GameObject[] bosses;
    public Item[] itemsCollection;

    float maxChance;
    // public GameObject base

    private void Start()
    {
        SetEvents();
        SetItemsStatus();
        // SpawnEnemy();
    }

    private void Update()
    {
        // if (Input.GetMouseButtonDown(1))
        // {
        //     SpawnItem();
        // }
    }

    void SetItemsStatus()
    {
        foreach (Item item in itemsCollection)
        {
            item.HaveItems = ListAllHaveItens(item);
            if (item.HaveItems.Length != 0)
            {
                item.Chance = new Vector2(maxChance, item.baseChance + maxChance);
                maxChance += item.baseChance;
            }
        }
    }

    void SetEvents()
    {
        GameManager.Instance.Main.OnAddLevel += SpawnItem;
    }

    GameObject[] ListAllHaveItens(Item collection)
    {
        // int n = 0;
        // GameObject[] haveItens = null;
        List<GameObject> haveItens = new List<GameObject>();
        foreach (var item in collection.items)
        {
            if (item.have) haveItens.Add(item.Item);
        }
        return haveItens.ToArray();
    }

    void SpawnItem()
    {
        float floatRnd = Random.Range(0f, maxChance);
        foreach (var item in itemsCollection)
        {
            if (floatRnd > item.Chance.x && floatRnd < item.Chance.y)
            {
                int intRnd = Random.Range(0, item.HaveItems.Length);
                Spawn(item.HaveItems[intRnd]);
                break;
            }
        }
        // var rnd = Random.Range(0, haveGuns.Length);
        // Spawn(gunsCollection.items[rnd].Item);
    }

    public async void SpawnEnemy()
    {
        if(!GameManager.Instance.Playing) return;
        if(GameManager.Instance.BossFight) return;
        Spawn(EnemyToSpawn());
        await new WaitForSeconds(Random.Range(2f, 4f));
        SpawnEnemy();
    }

    public void SpawnBoss(int boss){
        Spawn(bosses[boss]);
    }

    void Spawn(GameObject obj)
    {
        MoveSpawner();
        var go = GameManager.Instance.ObjectPooler.SpawnFromPool(obj, this.transform.position, this.transform.rotation);
        if (go.GetComponent<Bullet>())
        {
            var bullet = go.GetComponent<Bullet>();
            bullet.enabled = false;
        }
    }

    void MoveSpawner()
    {
        var variant = Random.Range(-1.75f, 1.75f);
        var posX = this.transform.position;
        posX[0] = variant;
        this.transform.position = posX;
    }

    GameObject EnemyToSpawn()
    {
        return baseEnemyJumper;
        // if level 5+ _ 20% chance spawn X enemy
    }

}

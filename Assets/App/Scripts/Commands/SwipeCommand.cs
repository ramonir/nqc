﻿using App.Commands;
using App.Core;
using UnityEngine;

namespace App.Commands {
    public class SwipeCommand : Command {

        Vector2 _secondPressPos;
        private Vector2 _currentSwipe;
        public Vector2 CurrentSwipe { get => _currentSwipe; }

        public override void Execute() {
            base.Execute();
            Swiping();
        }

        public void Swiping() {
            Vector2 firstPressPos = GameManager.Instance.InputHandler.TouchDownCommand.FirstPressPos;
            this._secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            this._currentSwipe = new Vector2(this._secondPressPos.x - firstPressPos.x, this._secondPressPos.y - firstPressPos.y);
        }

        public void TouchUp(){
            this._currentSwipe = new Vector2(0, 0);
        }

    }
}

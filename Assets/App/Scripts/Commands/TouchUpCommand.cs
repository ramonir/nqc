﻿using App.Commands;
using App.Core;
using UnityEngine;

namespace App.Commands {
    public class TouchUpCommand : Command {

        Vector2 _endPressPos;
        public Vector2 EndPressPos { get => _endPressPos; }
        public override void Execute() {
            base.Execute();
            TouchUp();
        }

        public void TouchUp() {
            this._endPressPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
            GameManager.Instance.InputHandler.SwipeCommand.TouchUp();
            // this.currentSwipe = new Vector2(0, 0);
        }
    }
}

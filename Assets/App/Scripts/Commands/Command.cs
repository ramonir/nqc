namespace App.Commands
{
    public abstract class Command
    {
        public delegate void CommandAction();
        public event CommandAction OnCommand;

        public virtual void Execute() { if (OnCommand != null) OnCommand(); }
    }
}
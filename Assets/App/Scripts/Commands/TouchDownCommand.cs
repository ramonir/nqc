﻿using App.Commands;
using App.Core;
using UnityEngine;

namespace App.Commands {
    public class TouchDownCommand : Command {

        Vector2 _firstPressPos;
        public Vector2 FirstPressPos { get => _firstPressPos; }

        public override void Execute () {
            base.Execute();
            TouchDown ();
        }

        public void TouchDown () {
            this._firstPressPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
        }
    }
}
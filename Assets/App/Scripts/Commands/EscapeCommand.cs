﻿using App.Commands;
using App.Core;
using UnityEngine;

namespace App.Commands {
    public class EscapeCommand : Command {
        public override void Execute() => Debug.Log("escape");
    }
}

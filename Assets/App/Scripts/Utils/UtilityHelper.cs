﻿public static class UtilityHelper
{
    public static string AdjustBigValue(int value)
    {
        if (value < 10000) return value.ToString();
        else return (value/1000).ToString("")+"k";
    }
}

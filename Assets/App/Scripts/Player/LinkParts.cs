﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using App.Guns;
using App.Structures;
using UnityEngine;

public class LinkParts : MonoBehaviour {
    [SerializeField]
    private List<GameObject> _parts = new List<GameObject> ();
    public List<GameObject> Parts { get => _parts; }

    public void OnTriggerEnter2D (Collider2D c) {
        var structure = c.GetComponent<PlayerStructure> ();
        if (structure) {
            if (structure.connected) return;
            var point = new GameObject ("PointParts").AddComponent<PointParts> ();
            _parts.Add (point.gameObject);
            point.transform.position = structure.transform.position;
            structure.transform.SetParent (point.transform);
            structure.connected = true;
            var gun = c.GetComponent<Gun> ();
            if (gun) gun.Shot ();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;

public class PointParts : MonoBehaviour {

    Vector2 startPos;
    Vector2 playerPos;
    Vector2 keyPos;
    Vector2 pos;

    float offset = 0.3f;

    private void Start () {
        startPos = transform.position;
        playerPos = GameManager.Instance.Player.transform.position;
        Reposition ();
    }

    private void Reposition () {
        Vector3 targetPos = playerPos;
        targetPos.x = targetPos.x - startPos.x;
        targetPos.y = targetPos.y - startPos.y;
        float angle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        
        transform.position = playerPos;
        transform.Translate(Vector2.left * offset);
        transform.localEulerAngles = Vector2.zero;

        startPos = transform.position;

        keyPos = new Vector2 (playerPos.x - startPos.x, playerPos.y - startPos.y);
    }

    private void Update () {
        playerPos = GameManager.Instance.Player.transform.position;
        pos = new Vector2 (playerPos.x - keyPos.x, playerPos.y - keyPos.y);
        transform.position = pos;
        transform.GetChild (0).transform.localEulerAngles = Vector2.zero;
        transform.GetChild (0).transform.localPosition = Vector2.zero;
        //foreach (var pos in GameObject.FindObjectsOfType<Gun>()) {
        //    pos.transform.localPosition = Vector2.zero;
        //}
    }

}
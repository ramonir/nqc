﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using App.Enemys;
using App.Structures;
using UnityEngine;
using UnityEngine.UI;

public class Player : PlayerStructure {
    [Header ("Player")]
    public float speed = 10;
    public GameObject defaultGun;
    public float defaultXPos = 2.1f;
    public float defaultYPos = -3.5f;

    protected override void OnEnable () {
        base.OnEnable ();
        GameManager.Instance.InputHandler.SwipeCommand.OnCommand += Move;
    }

    protected override void OnDisable () {
        base.OnDisable ();
        // GameManager.Instance.InputHandler.SwipeCommand.OnCommand -= Move;
    }

    void Update () {
        Fly ();
    }

    protected override void Die () {
        base.Die ();
        if ((int) life > 0) return;
        Destroy (GetComponentInChildren<Canvas> ().gameObject);
        Destroy (defaultGun.transform.parent.gameObject);
        _boxCol2D.enabled = false;
        GameManager.Instance.FinishGame ();
    }

    void LimitXMove () {
        var x = this.transform.position;
        if (this.transform.position.x > 1 && this.transform.position.x > defaultXPos) x[0] = defaultXPos;
        if (this.transform.position.x < -1 && this.transform.position.x < -defaultXPos) x[0] = -defaultXPos;
        this.transform.position = x;
    }

    void Fly () {
        var y = this.transform.position;
        if (!GameManager.Instance.Playing) {
            y[1] = Mathf.MoveTowards (y[1], defaultYPos, 2 * Time.deltaTime);
            this.transform.position = y;
            return;
        }
        var jetpack = GameManager.Instance.Main.ImpJetpack.Modifier + defaultYPos;
        if (this.transform.position.y < defaultYPos) y[1] = defaultYPos;
        else if (this.transform.position.y > jetpack) y[1] = jetpack;
        this.transform.position = y;
    }

    void Move () {
        if (!GameManager.Instance.Playing) return;
        Vector2 currentSwipe;
        currentSwipe = Camera.main.ScreenToWorldPoint (Input.mousePosition);
        transform.position = Vector3.MoveTowards (transform.position, currentSwipe, Time.deltaTime * speed);

        LimitXMove ();
        // Fly();
    }

}
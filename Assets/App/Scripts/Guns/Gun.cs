﻿using System.Collections;
using System.Collections.Generic;
using App.Bullets;
using App.Core;
using App.Enemys;
using App.Structures;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace App.Guns {
    public abstract class Gun : PlayerStructure {

        [Header ("Gun")]
        public bool enemyGun;
        public bool indestructible;
        public GameObject bullet;
        public Transform point;
        public int lvl;
        public float shotSpeed;
        protected float realSpeed;

        protected virtual void Start () {
            shotSpeed += shotSpeed * GameManager.Instance.Main.ImpSpeedGun.Modifier;
            realSpeed = 100 / shotSpeed;
        }

        protected override void OnEnable () {
            base.OnEnable ();
            if (transform.parent?.GetComponent<Player> () || transform.parent?.GetComponent<Enemy> ()) {
                if (transform.parent?.GetComponent<Enemy> ()) enemyGun = true;
                connected = true;
                indestructible = true;
                Rb.isKinematic = true;
                Shot();
            }
            if (indestructible) { 
                life = 0;
                Destroy (GetComponentInChildren<Canvas> ().gameObject); }
        }

        protected override void Die () {
            if (indestructible) return;
            base.Die ();
            // if (life > 0) return;
            // Destroy(gameObject);
        }

        public virtual void ChangeBullet (GameObject newBullet) {
            if (newBullet)
                bullet = newBullet;
        }

        abstract public void Shot ();

        protected override void OnTriggerEnter2D (Collider2D c) {
            base.OnTriggerEnter2D (c);
            if (c.GetComponent<Gun> ()) {
                var myGun = GetComponent<Gun> ().GetType ();
                var newGun = c.GetComponent<Gun> ().GetType ();
                if (myGun != newGun) return;
                if (!connected) Destroy (gameObject);
                else lvl++;
            }
            if (c.gameObject.tag == "BULLET") {
                if (enemyGun) return;
                var myBullet = bullet.GetComponent<Bullet> ();
                var newBullet = c.GetComponent<Bullet> ();
                if (newBullet.enabled) return;
                Destroy (c.gameObject);
                if (myBullet.GetType () != newBullet.GetType ()) {
                    ChangeBullet (Resources.Load ("Prefabs/Bullet/" + newBullet.GetType ().Name) as GameObject);
                } else myBullet.lvl++;
            }
        }

    }
}
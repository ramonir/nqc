﻿using System.Collections;
using System.Collections.Generic;
using App.Bullets;
using App.Core;
using UnityEngine;
namespace App.Guns {
    public class TripleGun : Gun {

        [Header ("Exclusive")]
        public Transform secondPoint;
        public Transform thirdPoint;

        public override void Shot () {
            StartCoroutine (Coroutine_Shot ());
        }

        IEnumerator Coroutine_Shot () {
            var go = GameManager.Instance.ObjectPooler.SpawnFromPool (bullet, point.position, point.rotation);
            var goBullet = go.GetComponent<Bullet> ();
            goBullet.enemyBullet = enemyGun;

            go = GameManager.Instance.ObjectPooler.SpawnFromPool (bullet, secondPoint.position, secondPoint.rotation);

            go = GameManager.Instance.ObjectPooler.SpawnFromPool (bullet, thirdPoint.position, thirdPoint.rotation);

            yield return new WaitForSeconds (realSpeed);
            Shot ();
        }
    }
}
﻿using App.Bullets;
using App.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Guns {
    public class ChargeGun : Gun {
        
        [Header("Exclusive")]
        [SerializeField] float multDamage = 5;
        [SerializeField] float multSpeed = 2;
        [SerializeField] float multScale = 2.5f;

        public override void Shot() {
            StartCoroutine(Coroutine_Shot());
        }

        IEnumerator Coroutine_Shot(){
            var go = GameManager.Instance.ObjectPooler.SpawnFromPool(bullet, point.position, point.rotation);
            var goBullet = go.GetComponent<Bullet>();
            goBullet.enemyBullet = enemyGun;
            goBullet.damage *= multDamage;
            goBullet.speed *= multSpeed;
            goBullet.transform.localScale *= multScale;
            yield return new WaitForSeconds(realSpeed);
            Shot();
        }
    }
}

﻿using App.Bullets;
using App.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace App.Guns {
    public class MultipleGun : Gun {

        [Header("Exclusive")]
        public GameObject secondBullet;
        public Transform secondPoint;

        public override void Shot() {
            StartCoroutine(Coroutine_Shot());
        }

        IEnumerator Coroutine_Shot(){
            var go = GameManager.Instance.ObjectPooler.SpawnFromPool(bullet, point.position, point.rotation);
            var goBullet = go.GetComponent<Bullet>();
            goBullet.enemyBullet = enemyGun;

            go = GameManager.Instance.ObjectPooler.SpawnFromPool(bullet, secondPoint.position, secondPoint.rotation);

            yield return new WaitForSeconds(realSpeed);
            Shot();
        }

        public override void ChangeBullet(GameObject newBullet) {
            base.ChangeBullet(newBullet);
            //secondBullet = newBullet;
        }
    }
}
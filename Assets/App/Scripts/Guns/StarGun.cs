﻿using System.Collections;
using System.Collections.Generic;
using App.Bullets;
using App.Core;
using UnityEngine;
namespace App.Guns {
    public class StarGun : Gun {

        [Header ("Exclusive")]
        public Transform[] points;

        public override void Shot () {
            StartCoroutine (Coroutine_Shot ());
        }

        IEnumerator Coroutine_Shot () {
            var go = GameManager.Instance.ObjectPooler.SpawnFromPool (bullet, point.position, point.rotation);
            var goBullet = go.GetComponent<Bullet> ();
            goBullet.enemyBullet = enemyGun;
            for (int i = 0; i < points.Length; i++) {
                go = GameManager.Instance.ObjectPooler.SpawnFromPool (bullet, points[i].position, points[i].rotation);
            }
            yield return new WaitForSeconds (realSpeed);
            Shot ();
        }
    }
}
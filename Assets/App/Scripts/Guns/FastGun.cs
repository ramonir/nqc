﻿using App.Bullets;
using App.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Guns {
    public class FastGun : Gun {

        [Header("Exclusive")]
        [SerializeField] float divDamage = 5;
        [SerializeField] float divScale = 2.5f;
        public override void Shot() {
            StartCoroutine(Coroutine_Shot());
        }

        IEnumerator Coroutine_Shot(){
            var go = GameManager.Instance.ObjectPooler.SpawnFromPool(bullet, point.position, point.rotation);
            var goBullet = go.GetComponent<Bullet>();
            goBullet.enemyBullet = enemyGun;
            goBullet.damage /= divDamage;
            goBullet.transform.localScale /= divScale;
            yield return new WaitForSeconds(realSpeed);
            Shot();
        }
    }
}

﻿using App.Bullets;
using App.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Guns {
    public class FervorGun : Gun {
        
        [Header("Exclusive")]
        [SerializeField] float resetTime;
        [SerializeField] float speedPerSecond;
        float _shotSpeed;

        protected override void Start() {
            base.Start();
            _shotSpeed = shotSpeed;
            ResetSpeed();
        }

        private void Update() {
            if (!connected) return;
            shotSpeed += Time.deltaTime * speedPerSecond;
            realSpeed = 100 / shotSpeed;
        }

        public override void Shot() {
            StartCoroutine(Coroutine_Shot());
        }

        IEnumerator Coroutine_Shot(){
            var go = GameManager.Instance.ObjectPooler.SpawnFromPool(bullet, point.position, point.rotation);
            var goBullet = go.GetComponent<Bullet>();
            goBullet.enemyBullet = enemyGun;
            yield return new WaitForSeconds(realSpeed);
            Shot();
        }

        async void ResetSpeed() {
            shotSpeed = _shotSpeed;
            await new WaitForSeconds(resetTime);
            ResetSpeed();
        }
    }
}

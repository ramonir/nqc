﻿using App.Bullets;
using App.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace App.Guns {
    public class ConsecutiveGun : Gun {

        public override void Shot() {
            StartCoroutine(Coroutine_Shot());
        }

        IEnumerator Coroutine_Shot(){
            var go = GameManager.Instance.ObjectPooler.SpawnFromPool(bullet, point.position, point.rotation);
            var goBullet = go.GetComponent<Bullet>();
            goBullet.enemyBullet = enemyGun;
            yield return new WaitForSeconds(0.1f);
            go = GameManager.Instance.ObjectPooler.SpawnFromPool(bullet, point.position, point.rotation);
            yield return new WaitForSeconds(0.1f);
            go = GameManager.Instance.ObjectPooler.SpawnFromPool(bullet, point.position, point.rotation);
            yield return new WaitForSeconds(realSpeed);
            Shot();
        }
    }
}
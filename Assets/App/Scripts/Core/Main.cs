﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;

public class Main : MonoBehaviour {
    [System.Serializable]
    public class Improve {
        [SerializeField] int basePrice;
        [SerializeField] float multPrice;
        [SerializeField] int _price;
        public int Price { get => _price; }

        [SerializeField] int _lvl;
        public int Lvl { get => _lvl; }
        [SerializeField] float modifierPerLvl;
        float _modifier;
        public float Modifier { get => _modifier; }

        public void SetStatus () {
            _modifier = _lvl * modifierPerLvl;
            _price = (int) (basePrice * (_lvl * multPrice));
        }

        public void Upgrade () {
            _lvl++;
            SetStatus ();
        }
    }

    [Header ("Level/Score")]
    [SerializeField] int _level = 0;
    public int Level { get => _level; }

    [SerializeField] int baseScoreToLvl = 1000;
    [SerializeField] private int score;

    [Header ("Gold")]
    [SerializeField] int _gold;
    public int Gold { get => _gold; }

    [Header ("Modifiers")]
    public Improve ImpSpeedGun;
    public Improve ImpDamageBullet;
    public Improve ImpJetpack;
    public Improve ImpCoin;

    // ------------------------------ EVENTS ------------------------------

    public delegate void AddLevelAction ();
    public event AddLevelAction OnAddLevel;

    private void Awake () {
        SetModifiers ();
    }
    void SetModifiers () {
        ImpSpeedGun.SetStatus ();
        ImpDamageBullet.SetStatus ();
        ImpJetpack.SetStatus ();
        ImpCoin.SetStatus ();
    }

    public void CheckHaveGold(){
        bool haveGold;
        if (!HaveGold (ImpSpeedGun.Price)) haveGold = false;
        else haveGold = true;
        UIManager.Instance.Improvements.StateButton(ExtraEnums.EnumImprovement.Gun, haveGold);

        if (!HaveGold (ImpDamageBullet.Price)) haveGold = false;
        else haveGold = true;
        UIManager.Instance.Improvements.StateButton(ExtraEnums.EnumImprovement.Bullet, haveGold);

        if (!HaveGold (ImpJetpack.Price)) haveGold = false;
        else haveGold = true;
        UIManager.Instance.Improvements.StateButton(ExtraEnums.EnumImprovement.Jetpack, haveGold);

        if (!HaveGold (ImpCoin.Price)) haveGold = false;
        else haveGold = true;
        UIManager.Instance.Improvements.StateButton(ExtraEnums.EnumImprovement.Coin, haveGold);
    }

    public void SendImproveValues(){
        UIManager.Instance.Improvements.SetImproveValues(ExtraEnums.EnumImprovement.Gun, ImpSpeedGun.Price, ImpSpeedGun.Lvl);
        UIManager.Instance.Improvements.SetImproveValues(ExtraEnums.EnumImprovement.Bullet, ImpDamageBullet.Price, ImpDamageBullet.Lvl);
        UIManager.Instance.Improvements.SetImproveValues(ExtraEnums.EnumImprovement.Jetpack, ImpJetpack.Price, ImpJetpack.Lvl);
        UIManager.Instance.Improvements.SetImproveValues(ExtraEnums.EnumImprovement.Coin, ImpCoin.Price, ImpCoin.Lvl);
    }

    public void UpgradeModifier (ExtraEnums.EnumImprovement modifier) {
        switch (modifier) {
            case ExtraEnums.EnumImprovement.Gun:
                if (!HaveGold (ImpSpeedGun.Price)) return;
                ImpSpeedGun.Upgrade ();
                UIManager.Instance.Improvements.SetImproveValues(ExtraEnums.EnumImprovement.Gun, ImpSpeedGun.Price, ImpSpeedGun.Lvl);
                break;
            case ExtraEnums.EnumImprovement.Bullet:
                if (!HaveGold (ImpDamageBullet.Price)) return;
                ImpDamageBullet.Upgrade ();
                UIManager.Instance.Improvements.SetImproveValues(ExtraEnums.EnumImprovement.Bullet, ImpDamageBullet.Price, ImpDamageBullet.Lvl);
                break;
            case ExtraEnums.EnumImprovement.Jetpack:
                if (!HaveGold (ImpJetpack.Price)) return;
                ImpJetpack.Upgrade ();
                UIManager.Instance.Improvements.SetImproveValues(ExtraEnums.EnumImprovement.Jetpack, ImpJetpack.Price, ImpJetpack.Lvl);
                break;
            case ExtraEnums.EnumImprovement.Coin:
                if (!HaveGold (ImpCoin.Price)) return;
                ImpCoin.Upgrade ();
                UIManager.Instance.Improvements.SetImproveValues(ExtraEnums.EnumImprovement.Coin, ImpCoin.Price, ImpCoin.Lvl);
                break;
        }
        UIManager.Instance.SetGold(_gold);
    }

    public void GainScore (float value, bool updateLevel = true) {
        score += (int) value;
        if(updateLevel) UpdateLevel ();
    }

    public void GainGold (float value) {
        _gold += (int) value;
    }

    public bool HaveGold (int value) {
        if (_gold < value) return false;
        _gold -= value;
        return true;
    }

    void UpdateLevel () {
        var pointsOut = (baseScoreToLvl * ((0.1f * _level) * _level)) + (baseScoreToLvl * (_level + 1));
        if (score / pointsOut > 1) AddLevel ();
    }

    void AddLevel () {
        _level++;
        if ((_level % 5) == 0) GameManager.Instance.StartBoss(_level);
        if (OnAddLevel != null) OnAddLevel ();
    }

}
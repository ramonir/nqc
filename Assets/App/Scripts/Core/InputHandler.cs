using UnityEngine;
using App.Commands;
namespace App.Core {
    public class InputHandler {

        private TouchDownCommand _touchDownCommand;
        public TouchDownCommand TouchDownCommand { get => _touchDownCommand; }
        private SwipeCommand _swipeCommand;
        public SwipeCommand SwipeCommand { get => _swipeCommand; }
        private TouchUpCommand _touchUpCommand;
        public TouchUpCommand TouchUpCommand { get => _touchUpCommand; }
        private EscapeCommand _escapeCommand;
        public EscapeCommand EscapeCommand { get => _escapeCommand; }

        public InputHandler() {
            this._touchDownCommand = new TouchDownCommand();
            this._swipeCommand = new SwipeCommand();
            this._touchUpCommand = new TouchUpCommand();
            this._escapeCommand = new EscapeCommand();
        }

        public Command Handle() {
            if (Input.GetMouseButtonDown(0)) return this._touchDownCommand;
            if (Input.GetMouseButton(0)) return this._swipeCommand;
            if (Input.GetMouseButtonUp(0)) return this._touchUpCommand;
            if (Input.GetKeyUp("escape")) return this._escapeCommand;

            return null; //TODO: add input logics to return command
        }

    }
}
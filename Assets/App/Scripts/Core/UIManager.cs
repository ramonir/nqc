﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace App.Core {
    public class UIManager : BaseManager<UIManager> {

        private Options _options;
        public Options Options { get => _options; }
        private Improvements _improvements;
        public Improvements Improvements { get => _improvements; }

        [SerializeField] RectTransform panel_startGame;
        [SerializeField] Button btnStart;
        [SerializeField] RectTransform panel_endGame;
        [SerializeField] Button btnEnd;
        [SerializeField] Button btnRestart;
        [SerializeField] TextMeshProUGUI txtCoin;

        private void Awake () {
            this._options = FindObjectOfType<Options>();
            this._improvements = FindObjectOfType<Improvements>();

            btnStart.onClick.AddListener (StartGame);
            btnEnd.onClick.AddListener(EndGame);
            btnRestart.onClick.AddListener(RestartGame);
        }

        private void Start () {
            DetectRestart ();
            GameManager.Instance.Main.CheckHaveGold();
            GameManager.Instance.Main.SendImproveValues();
            SetGold(GameManager.Instance.Main.Gold);
        }

        void DetectRestart () {
            if (!PlayerPrefs.HasKey (ExtraConst.RESTART_VALUE)) return;
            bool restart = Convert.ToBoolean (PlayerPrefs.GetInt (ExtraConst.RESTART_VALUE));
            if (restart) {
                StartCoroutine(Coroutine_RestartGame()); 
                PlayerPrefs.SetInt (ExtraConst.RESTART_VALUE, 0);
            }
        }

        IEnumerator Coroutine_RestartGame(){
            yield return new WaitForEndOfFrame();
            StartGame ();
        }

        void StartGame () {
            GameManager.Instance.StartGame ();
            panel_startGame.gameObject.SetActive (false);
            _improvements.rectT.gameObject.SetActive(false);
        }

        public void FinishGame(){
            panel_endGame.gameObject.SetActive (true);
            _improvements.rectT.gameObject.SetActive(true);
            GameManager.Instance.Main.CheckHaveGold();
            GameManager.Instance.Main.SendImproveValues();
            SetGold(GameManager.Instance.Main.Gold);
        }

        void EndGame(){
            SceneManager.LoadScene(ExtraConst.GAME_SCENE);
        }

        void RestartGame(){
            PlayerPrefs.SetInt(ExtraConst.RESTART_VALUE, 1);
            EndGame();
        }
        
        public void SetGold(int gold){
            txtCoin.text = UtilityHelper.AdjustBigValue(gold);
        }
    }
}
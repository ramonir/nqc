using System;
using System.Collections;
using App.Guns;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace App.Core {
    public class GameManager : BaseManager<GameManager> {

        private InputHandler _input;
        public InputHandler InputHandler { get => _input; }
        private Main _main;
        public Main Main { get => _main; }
        private Player _player;
        public Player Player { get => _player; }
        private Spawner _spawner;
        public Spawner Spawner { get => _spawner; }
        private ObjectPooler _objectPooler;
        public ObjectPooler ObjectPooler { get => _objectPooler; }

        bool _playing = false;
        public bool Playing { get => _playing; }

        bool _bossFight = false;
        public bool BossFight { get => _bossFight; }

        void Awake () {
            // this._main = AddObjectReference(_main);
            // this._swipe = AddNewReference(_swipe);
            this._input = new InputHandler ();
            this._main = FindObjectOfType<Main> ();
            this._player = FindObjectOfType<Player> ();
            this._spawner = FindObjectOfType<Spawner> ();
            this._objectPooler = FindObjectOfType<ObjectPooler> ();
        }

        public void StartGame () {
            _playing = true;
            _player.GetComponentInChildren<Gun> ().Rb.constraints = RigidbodyConstraints2D.None;
            _spawner.SpawnEnemy ();
        }

        public void StartBoss(int currentLevel){
            _bossFight = true;
            int boss = (currentLevel/5)-1;
            _spawner.SpawnBoss(boss);
        }

        public void FinishBoss(){
            _bossFight = false;
            _spawner.SpawnEnemy ();
        }

        public void FinishGame () {
            UIManager.Instance.FinishGame ();
            _playing = false;
            _bossFight = false;
        }

        void Update () {
            this._input?.Handle ()?.Execute ();
        }

    }
}
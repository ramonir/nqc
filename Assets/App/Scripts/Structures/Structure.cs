﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using App.Enemys;
using UnityEngine;
using UnityEngine.UI;

namespace App.Structures {
    public class Structure : MonoBehaviour {

        [Header ("Structure")]
        [SerializeField] protected float startLife = 5;
        protected float life;
        protected BoxCollider2D _boxCol2D;
        private Rigidbody2D _rb;
        public Rigidbody2D Rb { get => _rb; }
        protected SpriteRenderer _spriteRend;
        protected Text _txtLife;

        protected virtual void Awake () {
            _boxCol2D = GetComponent<BoxCollider2D> ();
            _rb = GetComponent<Rigidbody2D> ();
            _spriteRend = GetComponent<SpriteRenderer> ();
            _txtLife = GetComponentInChildren<Text> ();
        }

        protected virtual void OnEnable() {
            life = startLife;

            ModifyLifeText (life);
        }

        protected virtual void Die () {
            if ((int) life > 0) return;
            DestroyPart();
            if (!GetComponent<Player> ()) GameManager.Instance.ObjectPooler.Recicle (gameObject);
        }

        void DestroyPart () {
            var part = GetComponentInParent<PointParts> ();
            if (part == null) return; 
            for (int i = 0; i < part.transform.childCount; i++) {
                var gObj = part.transform.GetChild (i);
                GameManager.Instance.ObjectPooler.Recicle (gObj.gameObject);
                gObj.SetParent (null);
            }
            Destroy (part.gameObject);
        }

        protected void ModifyLifeText (float value) {
            if (_txtLife != null) _txtLife.text = ((int) value).ToString ();
        }

        public void GainLife (float value) {
            life += value;
            ModifyLifeText (life);
        }

        public virtual void Hit (float damage) {
            life -= damage;
            ModifyLifeText (life);
            Die ();
        }
    }
}
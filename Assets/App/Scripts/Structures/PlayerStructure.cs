﻿using System.Collections;
using System.Collections.Generic;
using App.Enemys;
using UnityEngine;

namespace App.Structures {
    public class PlayerStructure : Structure {

        [Header ("PlayerStructure")]
        public bool connected;
        LinkParts linkParts;

        protected override void Awake () {
            base.Awake ();
            if (GetComponent<LinkParts> ()) linkParts = GetComponent<LinkParts> ();
        }

        protected override void OnEnable () {
            base.OnEnable ();
        }

        protected virtual void OnDisable () {
            connected = false;
        }

        public override void Hit (float damage) {
            base.Hit (damage);
            if ((int) life > 0) Respire ();
        }
        async void Respire () {
            if (!this) return;
            _boxCol2D.enabled = false;
            _spriteRend.enabled = false;
            await new WaitForSeconds (0.2f);
            _spriteRend.enabled = true;
            await new WaitForSeconds (0.2f);
            _spriteRend.enabled = false;
            await new WaitForSeconds (0.2f);
            _spriteRend.enabled = true;
            await new WaitForSeconds (0.2f);
            _spriteRend.enabled = false;
            await new WaitForSeconds (0.2f);
            _spriteRend.enabled = true;
            _boxCol2D.enabled = true;
        }

        protected override void Die () {
            if ((int) life > 0) return;
            if (linkParts != null) {
                foreach (var part in linkParts.Parts) {
                    PlayerStructure item;
                    try { item = part.GetComponentInChildren<PlayerStructure> (); } catch { return; }
                    GainLife (item.life);
                    item.Hit (99999);
                }
                linkParts.Parts.Clear ();
            }
            base.Die ();
        }

        protected virtual void OnTriggerEnter2D (Collider2D c) {
            if (!connected) return;
            var enemy = c.GetComponent<Enemy> ();
            if (enemy) {
                Hit (enemy.ColDamage);
            }
        }
    }
}